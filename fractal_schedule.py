import datetime

from flask import Flask, render_template, request
from gw2api import GuildWars2Client


app = Flask(__name__)
fraclient = FractalAPIClient()

fractals = [
        {7, 26, 61, 76}, {5, 17, 32, 56, 77, 89}, {2, 36, 44, 62, 79, 91},
        {4, 31, 57, 66, 85}, {9, 22, 39, 58, 83}, {3, 27, 51, 68, 86, 93},
        {6, 21, 47, 69, 94}, {8, 29, 53, 81}, {14, 46, 65, 71, 96},
        {15, 34, 43, 55, 64, 82}, {1, 19, 28, 52, 92}, {18, 42, 72, 95},
        {10, 40, 70, 90}, {20, 35, 45, 60, 80}, {13, 30, 38, 63, 88, 97},
        {23, 48, 73, 98}, {24, 49, 74, 99}, {16, 41, 59, 87},
        {11, 33, 67, 84}, {12, 37, 54, 78}, {25, 50, 75, 100},
        ]

fractal_names = [
        "Aquatic Ruins", "Swampland", "Uncategorized",
        "Urban Battleground", "Molten Furnace", "Snowblind",
        "Cliffside", "Underground Facility", "Aetherblade",
        "Thaumanova Reactor", "Volcanic", "Captain Mai Trin",
        "Molten Boss", "Solid Ocean", "Chaos",
        "Nightmare", "Shattered Observatory", "Twilight Oasis",
        "Deepstone", "Siren's Reef", "Sunqua Peak"
        ]

class FractalAPIClient:

    def __init__(self):
        self.timestamp = datetime.datetime.utcnow()
        self.client = GuildWars2Client(version='v2')
        self.t4s = []
        self.recs = [[], []]
        self.get_fractal_data()

    def get_fractal_data(self):
        client = self.client
        reset_time = self.timestamp.replace(hour=00, minute=00, second=00) # 00:00 UTC
        if reset_time <= self.timestamp:
            # pylint: disable=no-member
            self.__clear_data()
            ids = [[achid['id'] for achid in client.achievementsdaily.get()['fractals']],
                   [achid['id'] for achid in client.achievementsdailytomorrow.get()['fractals']]]
            self.__parse_ids(ids)
            self.timestamp = datetime.datetime.utcnow()
        return self.t4s, self.recs

    def __parse_ids(self, ids):
        client = self.client
        names = [[daily['name'] for daily in client.achievements.get(ids=ids[0])],
                 [daily['name'] for daily in client.achievements.get(ids=ids[1])]]
        for i, names in enumerate(names):
            for n in names:
                if 'Tier 4' in n:
                    self.t4s.append(n.split('Tier 4 ')[1])
                if 'Recommended' in n:
                    rec_fractal = int(n.split('Scale ')[1])
                    for x, frac in enumerate(fractals):
                        if rec_fractal in frac:
                            frac_name = fractal_names[x]
                            break
                    self.recs[i].append(f'{frac_name} - {rec_fractal}')

    def __clear_data(self):
        self.t4s.clear()
        self.recs[0].clear()
        self.recs[1].clear()


fraclient = FractalAPIClient()


@app.route('/fractals/')
def home():
    if request.method == 'GET':
        t4s, recs = fraclient.get_fractal_data()
        return render_template('dailies.html',
                               d1t4s=t4s[:3],
                               d1recs=recs[0],
                               d2t4s=t4s[3:],
                               d2recs=recs[1])


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8003')
